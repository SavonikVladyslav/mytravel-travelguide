//
//  WeatherResponse.swift
//  MyTravel
//
//  Created by MACsimus on 10/15/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation

struct WeatherResponse: Codable {
    let latitude, longitude: Double
        let timezone: String?
        let currently: Currently?
//        let minutely: Minutely?
//        let hourly: Hourly?
        let daily: Daily?
//        let flags: Flags?
//        let offset: Int
}

// MARK: - Currently
public struct Currently: Codable {
    var time: Int?
//    let icon: Icon?
    let nearestStormDistance, nearestStormBearing: Int?
    let precipIntensity, precipProbability, temperature, apparentTemperature: Double?
    let dewPoint, humidity, pressure, windSpeed: Double?
    let windGust: Double?
    let windBearing: Int?
    let cloudCover: Double?
    let uvIndex: Int?
    let visibility, ozone: Double?
}

// MARK: - Daily
struct Daily: Codable {
    let summary: String?
//    let icon: Icon?
    let data: [DailyDatum]?
}

// MARK: - DailyDatum
struct DailyDatum: Codable {
    let time: Int?
    let summary: String?
//    let icon: Icon?
//    let sunriseTime, sunsetTime: Int
//    let moonPhase, precipIntensity, precipIntensityMax: Double
//    let precipIntensityMaxTime: Int
    let precipProbability: Double?
//    let precipType: PrecipType?
    let temperatureHigh: Double?
//    let temperatureHighTime: Int
    let temperatureLow: Double?
//    let temperatureLowTime: Int
    let apparentTemperatureHigh: Double?
//    let apparentTemperatureHighTime: Int
    let apparentTemperatureLow: Double?
//    let apparentTemperatureLowTime: Int
    let dewPoint, humidity, pressure, windSpeed: Double?
    let windGust: Double?
//    let windGustTime, windBearing: Int
    let cloudCover: Double?
//    let uvIndex, uvIndexTime: Int
    let visibility, ozone, temperatureMin: Double?
//    let temperatureMinTime: Int
    let temperatureMax: Double?
//    let temperatureMaxTime: Int
    let apparentTemperatureMin: Double?
//    let apparentTemperatureMinTime: Int
    let apparentTemperatureMax: Double?
//    let apparentTemperatureMaxTime: Int
}

