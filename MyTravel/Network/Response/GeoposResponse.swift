//
//  GeoposResponse.swift
//  MyTravel
//
//  Created by MACsimus on 10/16/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//


import Foundation

struct GeoposResponse: Codable {
    // MARK: - Code
    //struct Code: Codable {
    //    let rate: Rate?
    let results: [Result]?
    //    let status: Status?
    //    let stayInformed: StayInformed?
    //    let timestamp: Timestamp?
    //    let totalResults: Int?
    
    enum CodingKeys: String, CodingKey {
        //        case rate
        //        case status
        case results
        //        case stayInformed = "stay_informed"
        //        case timestamp
        //        case totalResults = "total_results"
    }
}

// MARK: - License
struct License: Codable {
    let name: String?
    let url: String?
}

// MARK: - Rate
struct Rate: Codable {
    let limit, remaining, reset: Int?
}

// MARK: - Result
struct Result: Codable {
    //    let annotations: Annotations?
    //    let bounds: Bounds?
    //    let components: Components?
    //    let confidence: Int?
    //    let formatted: String?
    let geometry: Geometry?
}

//// MARK: - Annotations
//struct Annotations: Codable {
//    let dms: Dms?
//    let mgrs, maidenhead: String?
//    let mercator: Mercator?
//    let osm: Osm?
//    let unM49: UnM49?
//    let callingcode: Int?
//    let currency: Currency?
//    let flag: Flag?
//    let geohash: String?
//    let qibla: Double?
//    let roadinfo: Roadinfo?
//    let sun: Sun?
//    let timezone: Timezone?
//    let what3Words: What3Words?
//    let wikidata: String?
//    let fips: FIPS?
//
//    enum CodingKeys: String, CodingKey {
//        case dms = "DMS"
//        case mgrs = "MGRS"
//        case maidenhead = "Maidenhead"
//        case mercator = "Mercator"
//        case osm = "OSM"
//        case unM49 = "UN_M49"
//        case callingcode, currency, flag, geohash, qibla, roadinfo, sun, timezone
//        case what3Words = "what3words"
//        case wikidata
//        case fips = "FIPS"
//    }
//}
//
//// MARK: - Currency
//struct Currency: Codable {
//    let alternateSymbols: [AteSymbol?]
//    let decimalMark: DecimalMark?
//    let htmlEntity: HTMLEntity?
//    let isoCode: ISOCode?
//    let isoNumeric: String?
//    let name: Name?
//    let smallestDenomination: Int?
//    let subunit: Subunit?
//    let subunitToUnit: Int?
//    let symbol: Symbol?
//    let symbolFirst: Int?
//    let thousandsSeparator: DecimalMark?
//    let disambiguateSymbol: AteSymbol?
//
//    enum CodingKeys: String, CodingKey {
//        case alternateSymbols = "alternate_symbols"
//        case decimalMark = "decimal_mark"
//        case htmlEntity = "html_entity"
//        case isoCode = "iso_code"
//        case isoNumeric = "iso_numeric"
//        case name
//        case smallestDenomination = "smallest_denomination"
//        case subunit
//        case subunitToUnit = "subunit_to_unit"
//        case symbol
//        case symbolFirst = "symbol_first"
//        case thousandsSeparator = "thousands_separator"
//        case disambiguateSymbol = "disambiguate_symbol"
//    }
//}
//
//enum AteSymbol: String, Codable {
//    case u0440 = "\\u0440."
//    case u0440U0443U0431 = "\\u0440\\u0443\\u0431."
//    case us = "US$"
//}
//
//enum DecimalMark: String, Codable {
//    case decimalMark = "."
//    case empty = ","
//}
//
//enum HTMLEntity: String, Codable {
//    case empty = "$"
//    case x20BD = "&#x20BD;"
//}
//
//enum ISOCode: String, Codable {
//    case rub = "RUB"
//    case usd = "USD"
//}
//
//enum Name: String, Codable {
//    case russianRuble = "Russian Ruble"
//    case unitedStatesDollar = "United States Dollar"
//}
//
//enum Subunit: String, Codable {
//    case cent = "Cent"
//    case kopeck = "Kopeck"
//}
//
//enum Symbol: String, Codable {
//    case empty = "$"
//    case u20Bd = "\\u20bd"
//}
//
//// MARK: - Dms
//struct Dms: Codable {
//    let lat, lng: String?
//}
//
//// MARK: - FIPS
//struct FIPS: Codable {
//    let county, state: String?
//}
//
//enum Flag: String, Codable {
//    case ud83CUddf7Ud83CUddfa = "\\ud83c\\uddf7\\ud83c\\uddfa"
//    case ud83CUddfaUd83CUddf8 = "\\ud83c\\uddfa\\ud83c\\uddf8"
//}
//
//// MARK: - Mercator
//struct Mercator: Codable {
//    let x, y: Double?
//}
//
//// MARK: - Osm
//struct Osm: Codable {
//    let editURL, url: String?
//
//    enum CodingKeys: String, CodingKey {
//        case editURL = "edit_url"
//        case url
//    }
//}
//
//// MARK: - Roadinfo
//struct Roadinfo: Codable {
//    let driveOn: DriveOn?
//    let speedIn: SpeedIn?
//
//    enum CodingKeys: String, CodingKey {
//        case driveOn = "drive_on"
//        case speedIn = "speed_in"
//    }
//}
//
//enum DriveOn: String, Codable {
//    case driveOnRight = "right"
//}
//
//enum SpeedIn: String, Codable {
//    case kmH = "km/h"
//    case mph = "mph"
//}
//
//// MARK: - Sun
//struct Sun: Codable {
//    let rise, sunSet: Rise?
//
//    enum CodingKeys: String, CodingKey {
//        case rise
//        case sunSet = "set"
//    }
//}
//
//// MARK: - Rise
//struct Rise: Codable {
//    let apparent, astronomical, civil, nautical: Int?
//}
//
//// MARK: - Timezone
//struct Timezone: Codable {
//    let name: String?
//    let nowInDst, offsetSEC: Int?
//    let offsetString, shortName: String?
//
//    enum CodingKeys: String, CodingKey {
//        case name
//        case nowInDst = "now_in_dst"
//        case offsetSEC = "offset_sec"
//        case offsetString = "offset_string"
//        case shortName = "short_name"
//    }
//}
//
//// MARK: - UnM49
//struct UnM49: Codable {
//    let regions: Regions?
//    let statisticalGroupings: [StatisticalGrouping?]
//
//    enum CodingKeys: String, CodingKey {
//        case regions
//        case statisticalGroupings = "statistical_groupings"
//    }
//}
//
//// MARK: - Regions
//struct Regions: Codable {
//    let easternEurope, europe, ru: String?
//    let world: String?
//    let americas, northernAmerica, us: String?
//
//    enum CodingKeys: String, CodingKey {
//        case easternEurope = "EASTERN_EUROPE"
//        case europe = "EUROPE"
//        case ru = "RU"
//        case world = "WORLD"
//        case americas = "AMERICAS"
//        case northernAmerica = "NORTHERN_AMERICA"
//        case us = "US"
//    }
//}
//
//enum StatisticalGrouping: String, Codable {
//    case medc = "MEDC"
//}
//
//// MARK: - What3Words
//struct What3Words: Codable {
//    let words: String?
//}
//
//// MARK: - Bounds
//struct Bounds: Codable {
//    let northeast, southwest: Geometry?
//}
//
// MARK: - Geometry
struct Geometry: Codable {
    let lat, lng: Double?
}
//
//// MARK: - Components
//struct Components: Codable {
//    let iso31661_Alpha2: ISO31661_Alpha2?
//    let iso31661_Alpha3: ISO31661_Alpha3?
//    let type: TypeEnum?
//    let city: String?
//    let continent: Continent?
//    let country: Country?
//    let countryCode: CountryCode?
//    let state: String?
//    let stateDistrict, county, stateCode, postcode: String?
//    let hamlet, village: String?
//
//    enum CodingKeys: String, CodingKey {
//        case iso31661_Alpha2 = "ISO_3166-1_alpha-2"
//        case iso31661_Alpha3 = "ISO_3166-1_alpha-3"
//        case type = "_type"
//        case city, continent, country
//        case countryCode = "country_code"
//        case state
//        case stateDistrict = "state_district"
//        case county
//        case stateCode = "state_code"
//        case postcode, hamlet, village
//    }
//}
//
//enum Continent: String, Codable {
//    case europe = "Europe"
//    case northAmerica = "North America"
//}
//
//enum Country: String, Codable {
//    case russia = "Russia"
//    case usa = "USA"
//}
//
//enum CountryCode: String, Codable {
//    case ru = "ru"
//    case us = "us"
//}
//
//enum ISO31661_Alpha2: String, Codable {
//    case ru = "RU"
//    case us = "US"
//}
//
//enum ISO31661_Alpha3: String, Codable {
//    case rus = "RUS"
//    case usa = "USA"
//}
//
//enum TypeEnum: String, Codable {
//    case city = "city"
//    case state = "state"
//    case village = "village"
//}
//
//// MARK: - Status
//struct Status: Codable {
//    let code: Int?
//    let message: String?
//}
//
//// MARK: - StayInformed
//struct StayInformed: Codable {
//    let blog, twitter: String?
//}
//
//// MARK: - Timestamp
//struct Timestamp: Codable {
//    let createdHTTP: String?
//    let createdUnix: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case createdHTTP = "created_http"
//        case createdUnix = "created_unix"
//    }
//}
