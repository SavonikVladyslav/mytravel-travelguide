//
//  NetworkManager.swift
//  MyTravel
//
//  Created by MACsimus on 10/15/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation

public typealias HTTPHeaders = [String: String]
public typealias HTTPBody = [String: Any]
protocol PRequest {
    var currentPath: String { get }
    var method: HTTPMethod { get }
    var header: HTTPHeaders? { get }
    var body: HTTPBody? { get }
    var onSuccess: ((Data) -> Void) { get }
    var onError: ((Error?) -> Void) { get }
}
class NetworkManager {
    class func sendRequest(_ request: PRequest) {
        guard let currentUrl = URL(string: request.currentPath) else { return }
        var currentRequest = URLRequest(url: currentUrl)
        currentRequest.httpMethod = request.method.rawValue
        currentRequest.allHTTPHeaderFields = request.header
        if let body = request.body, let jsonData = try? JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) {
            currentRequest.httpBody = jsonData
        }
        URLSession.shared.dataTask(with: currentRequest) { (data, response, error) in
            if let currentError = error { request.onError(currentError); return }
            if let currentData = data {
                if let currentJson = try? JSONSerialization.jsonObject(with: data!, options: []) { debugPrint(currentJson) }
                request.onSuccess(currentData)
            }
        }.resume()
    }
}
