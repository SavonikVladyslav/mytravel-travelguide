//
//  GeoposRequest.swift
//  MyTravel
//
//  Created by MACsimus on 10/16/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation
class GeoposRequest: PRequest {
    
    
    var currentPath: String = "https://api.opencagedata.com/geocode/v1/json?q=%@&key=1896dd4afd844426aa365542310b2694"
    var method: HTTPMethod = .get
    var header: HTTPHeaders?
    var body: HTTPBody?
    var onSuccess: ((Data) -> Void)
    var onError: ((Error?) -> Void)
    
    init(_ city: String,_ onComplite: @escaping ((GeoposResponse) -> Void),_ onError: @escaping ((Error?) -> Void)) {
        currentPath = String(format: currentPath, city)
        
        self.onError = onError
        self.onSuccess = { data in
            guard let decodedObject = try? JSONDecoder().decode(GeoposResponse.self, from: data) else { onError(nil); return }
            onComplite(decodedObject)
        }
    }
}




