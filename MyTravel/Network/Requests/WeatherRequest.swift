//
//  WeatherRequest.swift
//  MyTravel
//
//  Created by MACsimus on 10/15/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation

class WeatherRequest: PRequest {
    var currentPath: String = "https://api.darksky.net/forecast/7044e154e1d3f69ca43ed79e3e4f86a5/%@,%@?units=si"
    
    var method: HTTPMethod = .get
    var header: HTTPHeaders?
    var body: HTTPBody?
    var onSuccess: ((Data) -> Void)
    var onError: ((Error?) -> Void)
    
        init(_ latityde: String, _ longityde: String,_ onComplite: @escaping ((WeatherResponse) -> Void),_ onError: @escaping ((Error?) -> Void)) {
        currentPath = String(format: currentPath, latityde, longityde)
        self.onError = onError
        self.onSuccess = { data in
            guard let decodedObject = try? JSONDecoder().decode(WeatherResponse.self, from: data) else { onError(nil); return }
            onComplite(decodedObject)
        }
    }
}
