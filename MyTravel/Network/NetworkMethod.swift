//
//  NetworkMethod.swift
//  MyTravel
//
//  Created by MACsimus on 10/12/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation
enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"      // create a new content
    case patch = "PATCH"    // update a path of content
    case put = "PUT"        // update full content
    case delete = "DELETE"
}
