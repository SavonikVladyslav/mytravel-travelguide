//
//  Router.swift
//  Begunok-Executor
//
//  Created by Sergey Chuchukalo on 21.08.2019.
//  Copyright © 2019 Begunok. All rights reserved.
//

import UIKit

class Router {
    static func getRootViewController() -> UIViewController? {
        return Thread.isMainThread ? getRoot() : DispatchQueue.main.sync { return getRoot() }
    }
    private static func getRoot() -> UIViewController? {
        guard let rootVC = UIApplication.shared.keyWindow?.rootViewController else { return nil }
        return Router.getLastPresentedViewController(rootVC)
    }
    private static func getLastPresentedViewController(_ viewController: UIViewController) -> UIViewController {
        guard let presentedViewController = viewController.presentedViewController else { return viewController }
        return Router.getLastPresentedViewController(presentedViewController)
    }

    static func dismissVC(_ block: (() -> Void)?) { // hide all modal & show (push)
        guard let viewC = Router.getRootViewController() else { return }
        viewC.dismiss(animated: true, completion: block)
    }
    static func popVC(_ block: (() -> Void)?) {     // hide all push 
        guard let viewC = Router.getRootViewController() as? UINavigationController else { return }
        viewC.popViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            block?()
        })
    }
    static func popToRootVC(_ block: (() -> Void)?) { // return to first push screen
        guard let viewC = Router.getRootViewController() as? UINavigationController else { return }
        viewC.popToRootViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            block?()
        })
    }
}



