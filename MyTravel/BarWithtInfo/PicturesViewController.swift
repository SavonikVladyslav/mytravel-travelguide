//
//  PicturesViewController.swift
//  MyTravel
//
//  Created by MACsimus on 30.10.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit
                            // POP VIEW COONTROLLER
class PicturesViewController: ViewController {
    @IBOutlet weak var tableView: UITableView! { didSet { tableView.delegate = self; tableView.dataSource = self} }
    @IBOutlet weak var cityNameShow: UILabel!
    var cityName: String?
    
    override func viewDidLoad() {
        cityNameShow.text = cityName // KOSTYL' Need to be REFACTOR!!!!!!!!!!!!!!!!!!!!!!!
        
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ImageCellTableViewCell", bundle: .main), forCellReuseIdentifier: "ImageCellTableViewCell")
    }
}

extension PicturesViewController: UITableViewDelegate {
    //    ImageCellTableViewCell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

extension PicturesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCellTableViewCell") as? ImageCellTableViewCell {
            let curentName = (cityName ?? "").components(separatedBy: ",").first ?? "Kyiv"
            cell.setupView(name: "\(curentName)\(indexPath.row+1)")
            return cell
        }
        return UITableViewCell()
    }
}
