//
//  WeatherForecastViewController.swift
//  MyTravel
//
//  Created by MACsimus on 30.10.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class WeatherForecastViewController: ViewController {
    //@IBOutlet weak var tvWeather: UITableView!
    @IBOutlet weak var nameOfCity: UILabel!
    @IBOutlet weak var dateLabels: UIStackView!
    @IBOutlet weak var temperatureDay: UIStackView!
    @IBOutlet weak var temperatureNight: UIStackView!
    
    var info: TripInfo = TripInfo()
    var dateAndTemperatyre = [String: [Double]]()
   
    
    override func viewDidLoad() {

        super.viewDidLoad()
        nameOfCity.text = info.cityName
        initData()
    
    }
    
    func initData() {
        let temperatureHighLabels = temperatureDay.arrangedSubviews
        let temperatureLowLabels = temperatureNight.arrangedSubviews
        let dateLabel = dateLabels.arrangedSubviews
        let sortedKeys = Array(dateAndTemperatyre.keys).sorted(by: <)
        
        for i in 0..<sortedKeys.count {
            let temperatures = dateAndTemperatyre[sortedKeys[i]]
            
            (dateLabel[i] as! UILabel).text = sortedKeys[i]
            (temperatureHighLabels[i] as! UILabel).text = String(format: "%.1f", temperatures?[0] ?? 0)+"°C"
           (temperatureLowLabels[i] as! UILabel).text = String(format: "%.1f", temperatures?[1] ?? 0)+"°C"
        }
        
    }
    
    
    @IBAction func BackToBarWithInfo(_ sender: Any) {
        BarWithInfo.showVC()
    }
}
