//
//  BarWithPict&Info.swift
//  MyTravel
//
//  Created by MACsimus on 9/30/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class BarWithInfo: ViewController {
    
    @IBOutlet weak var tripName: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    
    //var onClose: ((String) -> Void)?
    var trip: TripInfo = TripInfo()
    var dateAndTemperatyre = [String: [Double]]()
    
    @IBAction func backToMyTrips(_ sender: Any) {
        Router.dismissVC(nil)

    }
    
    
    @IBAction func showMap(_ sender: Any) {
        MapScreenViewController.showVC()
    }
    // MARK: - Geopos & Weather -
    @IBAction func showWeatherForecast(_ sender: Any) {
        
       
        
//        //MARK: - Geopos Request -
//        let myGroup = DispatchGroup()
//        myGroup.enter()
        NetworkManager.sendRequest(GeoposRequest("\(trip.cityName ?? "Odessa"))", { (result) in
            
            let latityde = result.results?.first?.geometry?.lat ?? 0
            debugPrint(latityde)
            let longityde = result.results?.first?.geometry?.lng ?? 0
            debugPrint(longityde)
            //MARK: - Weather Request -
            NetworkManager.sendRequest(WeatherRequest("\(latityde)", "\(longityde)", { (result) in
                
                for i in 0...7 {
                    guard var temperatyreHigh = result.daily?.data?[i].temperatureHigh else { return }
                    guard var temperatyreLow = result.daily?.data?[i].temperatureLow else { return }
                    let date = self.dateFormatter(Double(result.daily?.data?[i].time ?? 0))
                    debugPrint(date)
                    debugPrint(temperatyreLow)
                    debugPrint(temperatyreHigh)
                    temperatyreHigh = Double(round(10*temperatyreHigh)/10)
                    temperatyreLow = Double(round(10*temperatyreLow)/10)
                    self.dateAndTemperatyre[date] = [temperatyreHigh,temperatyreLow]
                }
                DispatchQueue.main.async {
                    let view = WeatherForecastViewController()
                    
                    
                    if #available(iOS 13.0, *) {
                        view.modalPresentationStyle = .automatic
                    } else {
                        view.modalPresentationStyle = .popover
                    }
                    
                    view.info = self.trip
                    view.dateAndTemperatyre = self.dateAndTemperatyre
//                    view.modalPresentationStyle = .fullScreen
                    self.show(view, sender: self)
                }
//                myGroup.leave()
                debugPrint(self.dateAndTemperatyre)
            }, { (err) in
                debugPrint("Ne!To!V!Date")
            }))
            //–––––––––––––––––––––––––––
        }, { (err) in
            debugPrint("Cheto!Ne!To")
        }))
      
        
//        let view = WeatherForecastViewController()
//               if #available(iOS 13.0, *) {
//                   view.modalPresentationStyle = .automatic
//               } else {
//                   view.modalPresentationStyle = .popover
//               }
//               show(view, sender: self)

        
    }
    //––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
    
    func dateFormatter(_ date: TimeInterval) -> String {
        let input = Date(timeIntervalSince1970: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let convertedDate = formatter.string(from: input)
        return convertedDate
    }
    
    @IBAction func showPictures(_ sender: Any) {
        let view = PicturesViewController()
        view.cityName = trip.cityName
        if #available(iOS 13.0, *) {
            view.modalPresentationStyle = .automatic
        } else {
            view.modalPresentationStyle = .popover
        }
        show(view, sender: self)
    }
    
    
    @IBAction func showInfoAboutCity(_ sender: Any) {
        
        let view = CityInfoViewController()
        view.trip = trip
        if #available(iOS 13.0, *) {
            view.modalPresentationStyle = .automatic
        } else {
            view.modalPresentationStyle = .popover
        }
        show(view, sender: self)
    }
    
    func dayMonthYearFormat(_ ourDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: ourDate)
    }
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tripName.text = trip.tripName
        startDate.text = dayMonthYearFormat(trip.dateStart!)    // need to default
        endDate.text = dayMonthYearFormat(trip.dateEnd!)        //forsed
        
        
//        let view = MyTripsViewController()
//        view.onClose = { tripInfo in
//            let view = BarWithInfo()
//            view.trip  = tripInfo
//            view.modalPresentationStyle = .fullScreen
//            self.show(view, sender: self)
//        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
}
