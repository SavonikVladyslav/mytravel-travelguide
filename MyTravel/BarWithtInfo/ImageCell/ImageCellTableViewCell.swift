//
//  ImageCellTableViewCell.swift
//  MyTravel
//
//  Created by MACsimus on 05.11.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class ImageCellTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewCity: UIImageView!
    
    func setupView(name: String) {
        
        imageViewCity.image = UIImage(named: name.lowercased())
        
    }
}
