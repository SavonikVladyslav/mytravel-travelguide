//
//  CityInfoViewController.swift
//  MyTravel
//
//  Created by MACsimus on 30.10.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit
import WebKit
class CityInfoViewController: ViewController, WKUIDelegate {
    
    var webView: WKWebView!
    var trip: TripInfo = TripInfo()
    var city: String = ""
//    var urlRivne =  "https://en.wikipedia.org/wiki/Rivne"
//    var urlMoscow = "https://en.wikipedia.org/wiki/Moscow"
//    var urlKiev =   "https://en.wikipedia.org/wiki/Kiev"
//    var urlWarsaw = "https://en.wikipedia.org/wiki/Warsaw"
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        city = trip.cityName ?? "Odessa"
        let myURL = URL(string:"https://en.wikipedia.org/wiki/\(city)")
//        myURL = String(format: myURL, city)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
    
}



