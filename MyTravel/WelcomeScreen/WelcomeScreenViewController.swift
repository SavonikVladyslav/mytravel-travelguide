//
//  WelcomeScreen.swift
//  MyTravel
//
//  Created by MACsimus on 10/2/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: ViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func signIn(_ sender: Any) {
//        Router.dismissVC({ Router.dismissVC(nil)})
        let view = MyTripsViewController()
        view.modalPresentationStyle = .fullScreen
        show(view, sender: self)
        
    }
    
    
}
