//
//  SignUpViewController.swift
//  MyTravel
//
//  Created by MACsimus on 10/9/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class SignUpViewController: ViewController {
    
    
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var eMail: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var enterPassword: UITextField!
    @IBOutlet weak var reEnterPassword: UITextField!
    @IBOutlet private var Register: UIView!
    @IBOutlet private weak var switcherAcceptation: UISwitch!
    
    
    @IBAction func Register(_ sender: Any) {
        
        if login.text?.isEmpty ?? false || eMail.text?.isEmpty ?? false || phoneNumber.text?.isEmpty ?? false || enterPassword.text?.isEmpty ?? false || reEnterPassword.text?.isEmpty ?? false {
            self.alert(title: "Винимание", message: "Правильно ведите ваш логин и пароль")
        }
        
        if enterPassword.text != reEnterPassword.text {
            self.alert(title: "Винимание", message: "Пароли не совпадают")
        }
        
        
        
        if switcherAcceptation.isOn == true {   // add fields text in Registration Fields
            
            show(SignInViewController(), sender: nil)
            
        }
    }
    
    @IBAction func SignIn(_ sender: Any) {
        show(SignInViewController(), sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switcherAcceptation.isOn = false
        login.delegate = self
        eMail.delegate = self
        phoneNumber.delegate = self
        enterPassword.delegate = self
        reEnterPassword.delegate = self
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        // Do any additional setup after loading the view.
    }
    
}
