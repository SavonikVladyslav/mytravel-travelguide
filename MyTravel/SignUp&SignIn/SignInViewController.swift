//
//  SignInViewController.swift
//  MyTravel
//
//  Created by MACsimus on 10/9/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class SignInViewController: ViewController {
        
    @IBOutlet private weak var rememberMe: UISwitch!
    @IBOutlet private weak var login: UITextField!
    @IBOutlet private weak var password: UITextField!
    
    @IBAction func SignUp(_ sender: Any) {
        show(SignUpViewController(), sender: nil)
    }
    @IBAction func ForgotPassword(_ sender: Any) {
        show(ForgotPasswordViewController(), sender: nil)
    }
    @IBAction func Login(_ sender: Any) {
        
        var isPassCorrect = false
        var isLoginCorrect = false
        if  login.text?.isEmpty ?? false || password.text?.isEmpty ?? false  {
            self.alert(title: "Винимание", message: "Правильно ведите ваш логин и пароль")
        }
        
        if login.text == "Login" {
            login.layer.borderWidth = 1
            login.layer.borderColor = UIColor.green.cgColor
            isLoginCorrect = true
        }
        else if login.text != "Login" {
            login.layer.borderWidth = 1
            login.layer.borderColor = UIColor.red.cgColor
        }
        if password.text == "password" {               //ulesess cuz pass private
            password.layer.borderWidth = 1
            password.layer.borderColor = UIColor.green.cgColor
            isPassCorrect = true
            
        }
        else if password.text != "password" {
            password.layer.borderWidth = 1
            password.layer.borderColor = UIColor.red.cgColor
        }
        
        //Change if to enter
        if isPassCorrect == true && isLoginCorrect == true {
            show(MyTripsViewController(), sender: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rememberMe.isOn = false
        login.delegate = self
        password.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        }
    
}// ETO VSE ! The end Of Class SignInViewController





