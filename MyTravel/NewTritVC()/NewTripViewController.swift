//
//  NewTripViewController.swift
//  MyTravel
//
//  Created by MACsimus on 04.11.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit
struct TripInfo {
    var tripName: String?
    var dateStart: Date?
    var dateEnd: Date?
    var cityName: String?
}

class NewTripViewController: ViewController {
    
    @IBOutlet weak var createTripButton: UIButton!
    @IBOutlet weak var tripName: UITextField!
    @IBOutlet weak var nameOfCity: UILabel!
    @IBOutlet weak var startDate: UIDatePicker!
    @IBOutlet weak var endDate: UIDatePicker!
    
    var sourceArray: [Trip] = []
    var cityName: String?
    var onClose: ((TripInfo) -> Void)?
    
    
    
    @IBAction func startDateValueChange(_ sender: Any) {
        endDate.minimumDate = startDate.date
    }
    @IBAction func endDateValueChange(_ sender: Any) {
        //useless
    }
    @IBAction func backToFindCirtTV(_ sender: Any) {
        FindCityViewController.showVC()
    }
    
    @IBAction func createTrip(_ sender: Any) {
        let start = startDate.date
        let end = endDate.date
        guard var name = self.tripName.text else { return }
        let city = nameOfCity.text
        if name.isEmpty == true {
            name = "New trip"
        }
       
        let info = TripInfo(tripName: name, dateStart: start, dateEnd: end, cityName: city)
        let nameOfPict = ((info.cityName ?? "").components(separatedBy: ",").first?.lowercased() ?? "Kyiv")+"1"
        //image.image = UIImage(named: "\(nazva)1")"
        self.saveTrip(info.tripName ?? "", info.cityName ?? "", "\(String(describing: info.dateStart))", "\(String(describing: info.dateEnd))", nameOfPict)
        Router.dismissVC { self.onClose?(info) }
        
    }
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        nameOfCity.text = cityName
        startDate.minimumDate = Date()
        endDate.minimumDate = Date()
        // Do any additional setup after loading the view.
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        reloadSourceArray()
        
    }
    
}

extension NewTripViewController {
    
    func saveTrip(_ tripName: String, _ nameOfCity : String, _ tripStartDate: String, _ tripEndDate: String, _ pictures: String) {
        let newTrip: Trip? = CoreDataManager.newEntity()
        
        newTrip?.tripName = tripName
        newTrip?.city = nameOfCity
        newTrip?.startDate = tripStartDate
        newTrip?.endDate = tripEndDate
        newTrip?.pictures = pictures
        
        CoreDataManager.saveContext()
    }
    
    
    
    func reloadSourceArray() {                             // GAVNO !!!!!!!!!
        sourceArray = CoreDataManager.allEntry()
        createTripButton.addTarget(self, action: #selector(setter: createTripButton), for: .touchUpInside)
//        tableViewReloadDATA
    }
    
//    func deleteItem(_ trip: Trip) {
//        CoreDataManager.removeEntry(trip)
//        CoreDataManager.saveContext()
//        reloadSourseArray()
//    }
    
}
