//
//  ViewController+Navigation.swift
//  Begunok-Executor
//
//  Created by Sergey Chuchukalo on 21.08.2019.
//  Copyright © 2019 Begunok. All righ ts reserved.
//

import UIKit
extension ViewController {
    private enum VCShowingType {
        case present
        case push
        case modal
    }
    class func showVC() { showVC(.present, true) } // without SHTORKA
    class func pushVC() { showVC(.push, true) }     // navigation
    class func modalVC() { showVC(.modal, true) }   // With SHTORKA

    private class func showVC(_ presentType: VCShowingType,
                              _ animation: Bool,
                              completion: (() -> Void)? = nil) {
        guard let fromVC = Router.getRootViewController(),
            let presentVC = ViewController.createViewController(self.description()) else { return }
        let view = presentVC
        if #available(iOS 13.0, *) {
            view.isiOS13 = true
        } else {
            view.isiOS13 = false
        }
        switch presentType {
        case .modal:
            if #available(iOS 13.0, *) {
                view.modalPresentationStyle = .automatic
            } else {
                view.modalPresentationStyle = .fullScreen
            }
            fromVC.present(view, animated: animation, completion: completion)
        case .present:
            view.modalPresentationStyle = .fullScreen
            fromVC.present(view, animated: animation, completion: completion)
        case .push:
            if let viewController = fromVC as? UINavigationController {
                viewController.pushViewController(presentVC, animated: animation)
            } else {
                let navigation = UINavigationController(rootViewController: presentVC)
                navigation.modalPresentationStyle = .fullScreen
                fromVC.present(navigation, animated: true, completion: completion)
            }
        }
    }
    private class func createViewController(_ name: String) -> ViewController? {
        guard let classType = NSClassFromString(name) as? NSObject.Type,
            let viewController = classType.init() as? ViewController else { return nil }
        return viewController
    }
}
