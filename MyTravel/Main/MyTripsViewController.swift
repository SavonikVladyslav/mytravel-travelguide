//
//  MyTripsViewController.swift
//  MyTravel
//
//  Created by MACsimus on 10/9/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class MyTripsViewController: ViewController, UIScrollViewDelegate {
    var onClose: ((String) -> Void)?
    
    @IBOutlet weak var newTrip: UIButton!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var myTripView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tvScheduled: UITableView! { didSet { tvScheduled.delegate = self; tvScheduled.dataSource = self } }
    @IBOutlet weak var tvPast: UITableView! { didSet { tvPast.delegate = self; tvPast.dataSource = self } }
    
    var sourceArrayForPast: [Trip] = []
    var sourceArrayForCurrent: [Trip] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentControl.addTarget(self, action: #selector(segmentControllChange), for: .valueChanged)
        sourceArrayForPast = CoreDataManager.allEntry()
        sourceArrayForCurrent = CoreDataManager.allEntry()
        scrollView.delegate = self
        
    }
    
    
    
    @objc func segmentControllChange() {
        
        let width = Int(scrollView.frame.width) * segmentControl.selectedSegmentIndex
        scrollView.scrollRectToVisible(CGRect(origin: CGPoint(x: width, y: 0), size: scrollView.frame.size), animated: true)
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.x >= scrollView.frame.width {
            segmentControl.selectedSegmentIndex = 1
        } else {
            segmentControl.selectedSegmentIndex = 0
        }
        
    }
    
    @IBAction func newTrip(_ sender: Any) {
        
        let view = FindCityViewController()
        view.onClose = { city in
            let view = NewTripViewController()
            view.onClose = { tripInfo in
                
                CoreDataManager.saveContext()
                self.sourceArrayForPast = CoreDataManager.allEntry()    //reload
                self.sourceArrayForCurrent = CoreDataManager.allEntry() //reloader
                self.tvScheduled.reloadData()
                
                let view = BarWithInfo()
                view.trip = tripInfo
                view.modalPresentationStyle = .fullScreen
                self.show(view, sender: self)
            }
            view.cityName = city
            view.modalPresentationStyle = .fullScreen
            self.show(view, sender: self)
        }
        view.modalPresentationStyle = .fullScreen
        show(view, sender: self)
        
    }
    
    @IBAction func exploreMap(_ sender: Any) {
        MapScreenViewController.showVC()
    }
    
    private func deleteItem(_ trip: Trip) {
        CoreDataManager.removeEntry(trip)
        CoreDataManager.saveContext()
//        reloadSourseArray()
    }
    
//    private func reloadSourseArray() {
//    sourceArrayForCurrent = CoreDataManager.allEntry()
//        newTrip.addTarget(self, action: #selector(self), for: .touchUpInside)
//        newTrip.addTarget(self, action: #selector(<#T##@objc method#>), for: .touchUpInside)
//    tvScheduled.reloadData()
    
    
}



extension MyTripsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Router.dismissVC { self.onClose?(self.sourseArray[indexPath.row]) }
//        Router.dismissVC(self.onClose?(self.sourceArrayForCurrent/*[indexPath.row]*/))
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//          tableView.deselectRow(at: indexPath, animated: true)
//          Router.dismissVC { self.onClose?(self.sourseArray[indexPath.row]) }
//      }
    
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
//                if editingStyle == .delete {
//                    deleteItem(sourceArray[indexPath.row])
//                }
    }
}

extension MyTripsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tvScheduled: return sourceArrayForCurrent.count
        case tvPast: return sourceArrayForPast.count
        default: return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch tableView {
        case tvScheduled:
            cell.textLabel?.text = sourceArrayForCurrent[indexPath.row].city
        case tvPast:
            cell.textLabel?.text = sourceArrayForPast[indexPath.row].city
        default: break
        }
        return cell
    }
}

