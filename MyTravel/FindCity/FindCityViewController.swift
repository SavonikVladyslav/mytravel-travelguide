//
//  FindCityViewController.swift
//  MyTravel
//
//  Created by MACsimus on 10/10/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

class FindCityViewController: ViewController {
    
    
    
    var onClose: ((String) -> Void)?
    
    @IBOutlet weak var searchCity: UISearchBar! { didSet { searchCity.delegate = self } }
    @IBOutlet weak var tvFindCity: UITableView! { didSet { tvFindCity.delegate = self; tvFindCity.dataSource = self }
    }
    var sourseArray: [String] = []
    var allData: [String] = []
    
    
    @IBAction func backToMyTrip(_ sender: Any) {
        MyTripsViewController.showVC()
    }
    
    func importData() {
         view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
         if let filepath = Bundle.main.path(forResource: "MyCities", ofType: "txt") {
             do {
                let contents = try String(contentsOfFile: filepath)
                allData = contents.components(separatedBy: "\n")
                sourseArray = allData
                tvFindCity.reloadData()
                 
             } catch {
                 // contents could not be loaded
             }
         } else {
             // example.txt not found!
         }
     }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        importData()
        tvFindCity.allowsSelection = true
        
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
    }
    
}// end of class FindCityViewController

extension FindCityViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            sourseArray = allData
        } else {
            sourseArray = allData.filter({ (name) -> Bool in
                return name.lowercased().contains(searchText.lowercased())
            })
        }
        tvFindCity.reloadData()
    }
}




extension FindCityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Router.dismissVC { self.onClose?(self.sourseArray[indexPath.row]) }
    }
}



extension FindCityViewController: UITableViewDataSource {
    
    func tableView (_ tableViev: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourseArray.count
    }//end of func
    
    func tableView (_ tableViev: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = sourseArray[indexPath.row]
        return cell
        
    }//end of func
    
}





