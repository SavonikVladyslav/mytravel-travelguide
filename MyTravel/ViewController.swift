//
//  ViewController.swift
//  MyTravel
//
//  Created by MACsimus on 9/30/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    // MARK: - Init with coder -
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    /// is iOS13
    var isiOS13: Bool = true
    //––––––––––––––––––––––––––––––––––––––––
    // MARK: - Deinit -
    deinit { debugPrint("🔻Deinit \(type(of: self))") }
    //––––––––––––––––––––––––––––––––––––––––
    // MARK: - Init -
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        debugPrint("🔺Init \(type(of: self))")
    }
    
    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
        }
        
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }//end of alert
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }// end of hideKeyboard
    
}// the end of class ViewController

extension ViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
    
}

//func sendGeoposRequest () -> (latitude: String, longitude: String) {
//
//    return
//}
