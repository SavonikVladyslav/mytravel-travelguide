//
//  AppDelegate.swift
//  MyTravel
//
//  Created by MACsimus on 9/29/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.rootViewController = WelcomeScreenViewController() // Name of your ViewController
        window?.makeKeyAndVisible()
        return true
        }
    func applicationWillEnterForeground(_ application: UIApplication) {
        CoreDataManager.saveContext()
    }
    
    
}

