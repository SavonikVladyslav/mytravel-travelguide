//
//  MainScreenViewController.swift
//  MyTravel
//
//  Created by MACsimus on 9/29/19.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import UIKit
import MapKit

class MapScreenViewController: ViewController {
    
    @IBOutlet private weak var contantView: UIView!
    @IBOutlet private weak var image: UIImageView!
    @IBOutlet private weak var viewHeightLayout: NSLayoutConstraint!
    @IBOutlet private weak var mapView: MKMapView! { didSet { mapView.delegate = self } }
    
    var onClose: ((String) -> Void)?
    var cityName: String = ""
    var cityDict = [
        "Moscow": CLLocationCoordinate2D(latitude: 55.7504461, longitude: 37.6174943),
        "Kyiv": CLLocationCoordinate2D(latitude: 50.4500336, longitude: 30.5241361),
        "Rivne": CLLocationCoordinate2D(latitude: 50.6196175, longitude: 26.2513165),
        "Warsaw": CLLocationCoordinate2D(latitude: 52.2319237, longitude: 21.0067265)
    ]
    
    @IBAction func CreateTrip(_ sender: Any) {
        let view = NewTripViewController()
        view.onClose = { tripInfo in
            let view = BarWithInfo()
            view.trip = tripInfo
            view.modalPresentationStyle = .fullScreen
            self.show(view, sender: self)
//            Router.dismissVC({ Router.dismissVC(nil)})
//            Router.dismissVC { self.onClose?(info) }
//            Router.dismissVC(<#T##block: (() -> Void)?##(() -> Void)?##() -> Void#>)
        }
        view.cityName = mapView.selectedAnnotations.first?.title ?? "Kyiv"
        view.modalPresentationStyle = .fullScreen
        self.show(view, sender: self)
//        Router.dismissVC(<#T##block: (() -> Void)?##(() -> Void)?##() -> Void#>)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let guest = UISwipeGestureRecognizer(target: self, action: #selector(closeView))
        guest.direction = .down
        contantView.addGestureRecognizer(guest)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        var annotations: [MKPointAnnotation] = []
        for keyValue in cityDict {
            let annotation = MKPointAnnotation()
            annotation.title = keyValue.key
            annotation.coordinate = keyValue.value
            
            annotations.append(annotation)
        }
        
        mapView.showAnnotations(annotations, animated: true)
    }
    
    @objc private func closeView() {
        viewHeightLayout.constant = 0
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutSubviews()
        }
    }
}

extension MapScreenViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        annotationView.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        viewHeightLayout.constant = 150
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutSubviews()
        }
        guard let nazva = view.annotation?.title?!.lowercased() else {return}
        
        image.image = UIImage(named: "\(nazva)1")
    }
}
