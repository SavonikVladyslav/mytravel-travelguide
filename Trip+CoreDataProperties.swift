//
//  Trip+CoreDataProperties.swift
//  MyTravel
//
//  Created by MACsimus on 09.11.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//
//

import Foundation
import CoreData


extension Trip {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Trip> {
        return NSFetchRequest<Trip>(entityName: "Trip")
    }

    @NSManaged public var city: String?
    @NSManaged public var startDate: String?
    @NSManaged public var endDate: String?
    @NSManaged public var pictures: String?
    @NSManaged public var tripName: String?

}
