//
//  CoreDataManager.swift
//  MyTravel
//
//  Created by MACsimus on 09.11.2019.
//  Copyright © 2019 VladyslavSavonik. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    static var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CoreData")
        container.loadPersistentStores { ( _, _) in }
        return container
    }()
    class func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges { try? context.save() }
    }
    class func allEntry<T: NSManagedObject>() -> [T] {
        let context = persistentContainer.viewContext
        guard let result = try? context.fetch(NSFetchRequest<T>(entityName: T.description())) else { return [] }
        return result
    }
    class func newEntity<T: NSManagedObject>() -> T? {
        let context = persistentContainer.viewContext
        guard let entity = NSEntityDescription.entity(forEntityName: T.description(), in: context),
            let entityTClass = NSManagedObject(entity: entity, insertInto: context) as? T else { return nil }
        return entityTClass
    }
    class func removeEntry(_ entry: NSManagedObject) { // remove  
        let context = persistentContainer.viewContext
        context.delete(entry)
    }
    class func removeTemporaryEntry() { // remove what i dont save
        persistentContainer.viewContext.rollback()
    }
    class func stepBack() {
        persistentContainer.viewContext.undo()
    }
    class func stepForward() {
        persistentContainer.viewContext.redo()
    }
}
